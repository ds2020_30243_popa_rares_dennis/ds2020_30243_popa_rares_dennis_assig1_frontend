import React from "react";

import MedicationTable from "../medic/components/medication-table";
import MedicationPlanTable from "../medic/components/medicationPlan-table";
import * as API_CAREGIVER from "./api/caregiverAPI"
import {Card, CardHeader, Col, Row} from 'reactstrap';
import PatientTable from "../medic/components/patient-table";
import {HOST} from '../commons/api/hosts';

import SockJsClient from 'react-stomp';



class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);
        this.user = this.props.userState;
        this.updateList = this.updateList.bind(this);
        this.state = {
            caregiver: [],
            medicationPlanTableData: [],
            messages: [],
            medicationTableData: [],
            patientTableData:[],
            caregiverLoaded: false,
            stompClient : null
        }
    }


    componentDidMount() {
        this.fetchCaregiver();
    }

    fetchCaregiver() {
        return API_CAREGIVER.getCaregiverById(this.user.id, (result, status, err) => {
            if (result !== null) {
                console.log(result);
                let plansList = this.createMedicationPlansList(result["patients"]);
                this.setState({
                    caregiver: result,
                    medicationPlanTableData: this.getMedicationPlanTableData(plansList),
                    medicationTableData: this.getMedicationTableData(plansList),
                    patientTableData: this.getPatientTableData(result["patients"]),
                    caregiverLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    updateList(){
       let message = this.state.messages;
       let ret = [];
       console.log("Update Se face ?");
       for(let i=0;i<message.length;i++) {
           if (message[i].name === "sleeping") {
               console.log("APEL2");
               let hours = (message[i].finishingTime - message[i].startingTime) / (60000.0 * 60.0);
               let minutes = (hours - Math.floor(hours)) * 60.0;
               hours = Math.floor(hours);
               minutes = Math.floor(minutes);
               let text = "Patient has overslept for " + hours + " hours and " + minutes + " minutes";
               ret.push(<li key={i}> {text} </li>);
           }
       }
       return ret;
    }

    createMedicationPlansList(patientsList){
        let medicationPlans = [];
        for(let i=0;i<patientsList.length;i++){
            for(let j=0;j<patientsList[i]["medicationPlans"].length;j++){
                medicationPlans.push(patientsList[i]["medicationPlans"][j]);
            }
        }
        return medicationPlans;
    }

    getPatientTableData(result) {
        let patientArray = [];
        for (let i = 0; i < result.length; i++) {
            let MedicationPlans = "";
            for (let j = 0; j < result[i]["medicationPlans"].length; j++) {
                MedicationPlans += result[i]["medicationPlans"][j]["id"] + " ";
            }
            let patient = {
                id: result[i]["id"],
                patientFirstName: result[i]["user"]["firstName"],
                patientLastName: result[i]["user"]["lastName"],
                adress: result[i]["user"]["adress"],
                email: result[i]["user"]["email"],
                gender: result[i]["user"]["gender"],
                medicationPlans: MedicationPlans,
                medicalRecord: result[i]["medicalRecord"]
            }
            patientArray[i] = patient;
        }
        return patientArray;
    }

    getMedicationPlanTableData(result) {
        let medicationPlansArray = [];
        for (let i = 0; i < result.length; i++) {
            let medications = "";
            for (let j = 0; j < result[i]["medications"].length; j++) {
                medications += result[i]["medications"][j]["id"] + " ";
            }
            let medicationPlan = {
                id: result[i]["id"],
                startTime: result[i]["startTime"].split(" ")[1],
                endTime: result[i]["endTime"].split(" ")[1],
                startDate: result[i]["startDate"],
                endDate: result[i]["endDate"],
                medications: medications
            }
            medicationPlansArray[i] = medicationPlan;
        }
        return medicationPlansArray;
    }

    getMedicationTableData(result) {
        let medicationArray = [];
        for (let i = 0; i < result.length; i++) {
            for (let j = 0; j < result[i]["medications"].length; j++) {
                medicationArray.push(result[i]["medications"][j]);
            }
        }
        return medicationArray;
    }


    reload() {
        this.setState({
            caregiverLoaded: false
        });

        this.fetchCaregiver();
    }


    render() {

        return (
            <div>

                <SockJsClient url= {HOST.backend_api + '/sd-websocket-medical-platform/'}
                              topics={['/topic/events']}
                              onConnect={() => {
                                  console.log("connected");
                              }}
                              onDisconnect={() => {
                                  console.log("Disconnected");
                              }}
                              onMessage={(msg) => {
                                  console.log("MESAJ:");
                                  console.log(msg);
                                  let mesgs = this.state.messages;
                                  mesgs.push(msg);
                                  this.setState({messages:mesgs});
                              }}
                              />

                <CardHeader>
                    <strong> Hello {this.user.firstName + " " + this.user.lastName} </strong>
                </CardHeader>

                <ul>
                    {this.updateList()}
                </ul>

                <Card>
                    <h3>PatientTable</h3>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}} className="patientTable">
                            {this.state.caregiverLoaded && <PatientTable tableData={this.state.patientTableData}/>}
                        </Col>
                    </Row>
                </Card>

                <Card>
                    <h3>Medication Table</h3>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}} className="medicationTable">
                            {this.state.caregiverLoaded &&
                            <MedicationTable tableData={this.state.medicationTableData}/>}
                        </Col>
                    </Row>
                </Card>

                <Card>
                    <h3>MedicationPlanTable</h3>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}} className="medicationPlanTable">
                            {this.state.caregiverLoaded &&
                            <MedicationPlanTable tableData={this.state.medicationPlanTableData}/>}
                        </Col>
                    </Row>
                </Card>


            </div>
        )

    }
}

export default CaregiverContainer;