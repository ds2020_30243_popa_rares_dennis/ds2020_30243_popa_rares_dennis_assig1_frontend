import {HOST} from '../../commons/api/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiver: '/caregiver'
};

function getCaregiverById(userId,callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/findByUser/' + userId, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}



export {
    getCaregiverById,
};