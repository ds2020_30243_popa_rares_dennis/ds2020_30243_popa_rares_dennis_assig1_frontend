import React from "react";
import LoginForm from "./login/LoginForm";
import MedicContainer from "./medic/medic";
import './commons/styles/project-style.css'
import styles from './commons/styles/project-style.css';
import {Route, Router, Switch} from "react-router-dom";
import history from './history';
import PatientContainer from "./patient/patient";
import CaregiverContainer from "./caregiver/caregiver";


class App extends React.Component {

    constructor(props) {

        super(props);
        this.handler = this.handler.bind(this);

        this.state = {
            user: {
                id: -1,
                firstName: "",
                lastName: "",
                birthdate: new Date(),
                gender: "",
                adress: "",
                email: "",
                password: "",
                passwordConfirm: "",
                role: ""
            },
            logged: false
        }


    }

    componentDidMount() {
        if (this.state.user.role === "MEDIC")
            history.push("/medic");
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //if(this.state.user.role === "MEDIC")
        //history.push("/medic");
    }

    handler(userData) {
        this.setState(userData);

        console.log("I am back into app and the state is:");
        console.log(this.state);
    }

    updateWindows() {

        let loginPage = (<Route
            exact
            path='/'
            render={() => <LoginForm handler={this.handler}
                                     userState={this.state}/>}
        />);

        let medicPage = (< Route
            path='/'
            render={() => <MedicContainer userState={this.state.user}/>}
        />);

        let patientPage = (< Route
            path='/'
            render={() => <PatientContainer userState={this.state.user}/>}
        />);

        let caregiverPage =  (< Route
            path='/'
            render={() => <CaregiverContainer userState={this.state.user}/>}
        />);

        switch(this.state.user.role){
            case "" : return loginPage;
            case "MEDIC" : return medicPage;
            case "PATIENT": return patientPage;
            case "CAREGIVER": return caregiverPage;
            default : return loginPage;
        }

    }

    render() {


        return (
            <div className={styles.back}>

                <Router history={history}>
                    <Switch>

                        {this.updateWindows()}

                    </Switch>
                </Router>
            </div>
        );
    }
}

export default App;
