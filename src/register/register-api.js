import {HOST} from '../commons/api/hosts';
import RestApiClient from "../commons/api/rest-client";

const endpoint = {
    medic: '/medic'
};


function postMedic(medic, callback){
    let request = new Request(HOST.backend_api + endpoint.medic + '/insert' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medic)
    });

    console.log("Body: " + JSON.stringify(medic));
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    postMedic
};
