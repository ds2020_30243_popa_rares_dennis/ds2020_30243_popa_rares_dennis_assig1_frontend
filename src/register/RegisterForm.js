import React from "react";
import {Col, Input, Label, Row} from "reactstrap";
import Button from "react-bootstrap/Button";
import '../login/formStyles/formStyles.css'
import * as API_USERS from "./register-api";

class RegisterForm extends React.Component {

    constructor(props) {

        super(props);
        this.onRegister = this.onRegister.bind(this);
        this.onChange = this.onChange.bind(this);

        this.state = {
            firstName: "",
            lastName: "",
            birthdate: new Date(),
            gender: "",
            adress: "",
            email: "",
            password: "",
            passwordConfirm: ""
        }

    }

    render() {

        return (
            <div className="loginForm">

                <div id='firstName' className="firstNameClass">
                    <Label for='firstNameField'> First Name </Label>
                    <Input name='firstName' id='firstNameField' onChange={this.onChange}/>
                </div>

                <div id='lastName' className="lastNameClass">
                    <Label for='lastNameField'> Last Name </Label>
                    <Input name='lastName' id='lastNameField' onChange={this.onChange}/>
                </div>


                <div id='birthdate' className="birthdateClass">
                    <Label htmlffor='birthdateField'> Birthdate </Label>
                    <Input type="date" name='birthdate' id='birthdateField' onChange={this.onChange}/>
                </div>

                <div id='gender' className="genderClass">
                    <label htmlFor='genderField'> Gender </label>
                    <select name="gender" id="genderField" onChange={this.onChange}>
                        <option value="MALE"> Male</option>
                        <option value="FEMALE"> Female</option>
                    </select>
                </div>


                <div id='address' className="addressClass">
                    <Label for='addressField'> Address </Label>
                    <Input name='address' id='addressField' onChange={this.onChange}/>
                </div>

                <div id='email' className="emailClass">
                    <Label for='emailField'> Email </Label>
                    <Input name='email' id='emailField' onChange={this.onChange}/>
                </div>

                <div id='password' className="passwordClass">
                    <Label for='passwordField'> Password </Label>
                    <Input type="password" name='password' id='passwordField' onChange={this.onChange}/>
                </div>

                <div id='passwordConfirm' className="passwordConfirmClass">
                    <Label for='passwordConfirmField'> Password Confirm </Label>
                    <Input type="password" name='passwordConfirm' id='passwordConfirmField'
                           onChange={this.onChange}/>
                </div>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={this.onRegister}> Register </Button>
                    </Col>
                </Row>


            </div>
        )
    }

    onRegister() {

        let medic = {

            user : {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                birthdate: this.state.birthdate,
                gender: this.state.gender,
                adress: this.state.adress,
                email: this.state.email,
                password: this.state.password,
                role: "MEDIC"
            }
        }

        return API_USERS.postMedic(medic, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted person with id: " + result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    onChange = event => {

        const name = event.target.name;
        const value = event.target.value;
        let auxState = this.state;

        switch (name) {
            case "firstName" :
                auxState.firstName = value;
                break;
            case "lastName" :
                auxState.lastName = value;
                break;
            case "birthdate" :
                auxState.birthdate = value;
                break;
            case "gender" :
                auxState.gender = value;
                break;
            case "adress" :
                auxState.adress = value;
                break;
            case "email" :
                auxState.email = value;
                break;
            case "password" :
                auxState.password = value;
                break;
            case "passwordConfirm" :
                auxState.passwordConfirm = value;
                break;
            default :
                break;
        }

        this.setState(auxState);

        console.log(name + "=" + value);

    }


}

export default RegisterForm;