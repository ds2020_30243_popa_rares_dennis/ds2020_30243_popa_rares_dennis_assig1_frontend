import {HOST} from '../../commons/api/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient'
};

function getPatientById(userId,callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + '/findByUser/' + userId, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}



export {
    getPatientById,
};