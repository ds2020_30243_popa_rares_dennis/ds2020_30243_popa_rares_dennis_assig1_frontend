import React from "react";


import MedicationTable from "../medic/components/medication-table";
import MedicationPlanTable from "../medic/components/medicationPlan-table";
import * as API_PATIENT from "./api/patientAPI"
import {Card, CardHeader, Col, Row} from 'reactstrap';

class PatientContainer extends React.Component {

    constructor(props) {
        super(props);
        this.user = this.props.userState;
        this.state = {
            patient:[],
            medicationPlanTableData:[],
            medicationTableData:[],
            patientLoaded:false,

        }
    }

    componentDidMount() {
        this.fetchPatient();
    }

    fetchPatient(){
        return API_PATIENT.getPatientById(this.user.id,(result, status, err) => {
            if (result !== null) {
                console.log(result);
                this.setState({
                    patient: result,
                    medicationPlanTableData:this.getMedicationPlanTableData(result["medicationPlans"]),
                    medicationTableData:this.getMedicationTableData(result["medicationPlans"]),
                    patientLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    getMedicationPlanTableData(result){
        let medicationPlansArray = [];
            for (let i = 0; i < result.length; i++) {
                let medications = "";
                for (let j = 0; j < result[i]["medications"].length; j++) {
                    medications += result[i]["medications"][j]["id"] + " ";
                }
                let medicationPlan = {
                    id: result[i]["id"],
                    startTime: result[i]["startTime"].split(" ")[1],
                    endTime: result[i]["endTime"].split(" ")[1],
                    startDate: result[i]["startDate"],
                    endDate: result[i]["endDate"],
                    medications: medications
                }
                medicationPlansArray[i] = medicationPlan;
            }
            return medicationPlansArray;
    }

    getMedicationTableData(result){
        let medicationArray = [];
        for (let i = 0; i < result.length; i++) {
            for (let j = 0; j < result[i]["medications"].length; j++) {
                medicationArray.push(result[i]["medications"][j]);
            }
        }
        return medicationArray;
    }




    reload() {
        this.setState({
            patientLoaded:false
        });

        this.fetchPatient();
    }


    render() {

        return (
            <div>

                <CardHeader>
                    <strong> Hello {this.user.firstName + " " + this.user.lastName} </strong>
                </CardHeader>

                <Card>
                    <h3>Medication Table</h3>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}} className="medicationTable">
                            {this.state.patientLoaded &&
                            <MedicationTable tableData={this.state.medicationTableData}/>}
                        </Col>
                    </Row>
                </Card>

                <Card>
                    <h3>MedicationPlanTable</h3>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}} className="medicationPlanTable">
                            {this.state.patientLoaded &&
                            <MedicationPlanTable tableData={this.state.medicationPlanTableData}/>}
                        </Col>
                    </Row>
                </Card>


            </div>
        )

    }
}

export default PatientContainer;