import {HOST} from '../../commons/api/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    medication: '/medication'
};


function getMedication(callback) {
    let request = new Request(HOST.backend_api + endpoint.medication + '/getMedications', {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function postMedication(medication, callback){
    let request = new Request(HOST.backend_api + endpoint.medication + '/insert' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateMedication(medication, callback){
    let request = new Request(HOST.backend_api + endpoint.medication + '/updateMedication' , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedication(medication, callback){
    let request = new Request(HOST.backend_api + endpoint.medication + '/deleteMedication/' + medication , {
        method: 'DELETE'
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedication,
    postMedication,
    updateMedication,
    deleteMedication
};