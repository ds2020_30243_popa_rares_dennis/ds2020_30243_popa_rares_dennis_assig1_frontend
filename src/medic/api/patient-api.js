import {HOST} from '../../commons/api/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient'
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + '/getPatients', {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(patient, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + '/insert' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updatePatient(patient, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + '/updatePatient' , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePatient(patient, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + '/delete/' + patient , {
        method: 'DELETE'
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function addPlanToPatient(patientId,planId, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + '/addMedicationPlan/' + patientId + "/" + planId , {
        method: 'PUT',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function removePlanFromPatient(patientId,planId, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + '/removeMedicationPlan/' + patientId + "/" + planId , {
        method: 'PUT',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getPatients,
    postPatient,
    updatePatient,
    deletePatient,
    addPlanToPatient,
    removePlanFromPatient
};