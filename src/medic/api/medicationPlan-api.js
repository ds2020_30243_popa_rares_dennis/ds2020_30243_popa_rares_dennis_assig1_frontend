import {HOST} from '../../commons/api/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    medicationPlan: '/medicationPlan'
};


function getMedicationPlans(callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + '/getAllMedicationPlans', {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedicationPlan(medicationPlan, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + '/insert' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationPlan)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateMedicationPlan(medicationPlan, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + '/updateMedicationPlan' , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationPlan)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function addMedicationToPlan(medicationPlanId,medicationId, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + '/addMedication/' + medicationPlanId + "/" + medicationId , {
        method: 'PUT',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function removeMedicationFromPlan(medicationPlanId,medicationId, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + '/removeMedication/' + medicationPlanId + "/" + medicationId , {
        method: 'PUT',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedicationPlans,
    postMedicationPlan,
    updateMedicationPlan,
    addMedicationToPlan,
    removeMedicationFromPlan
};