import {HOST} from '../../commons/api/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiver: '/caregiver'
};


function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/getCaregivers', {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postCaregiver(caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/insert' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateCaregiver(caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/updateCaregiver' , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/deleteCaregiver/' + caregiver , {
        method: 'DELETE'
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


function addPatientToCaregiver(caregiverId,patientId, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/addPatient/' + caregiverId + "/" + patientId , {
        method: 'PUT',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function removePatientFromCaregiver(caregiverId,patientId, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/removePatient/' + caregiverId + "/" + patientId , {
        method: 'PUT',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getCaregivers,
    postCaregiver,
    updateCaregiver,
    deleteCaregiver,
    addPatientToCaregiver,
    removePatientFromCaregiver
};