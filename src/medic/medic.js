import React from "react";
import * as API_MEDICATION from "./api/medication-api"
import * as API_CAREGIVER from "./api/caregiver-api"
import * as API_PATIENT from "./api/patient-api"
import * as API_MEDICATIONPLAN from "./api/medicationPlan-api"

import MedicationTable from "./components/medication-table";
import CaregiverTable from "./components/caregiver-table";
import PatientTable from "./components/patient-table";
import MedicationPlanTable from "./components/medicationPlan-table";

import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from 'reactstrap';
import PersonForm from "./components/AddPatientForm";
import UpdateUserForm from "./components/UpdateUserForm";
import DeleteUserForm from "./components/DeleteUserForm";
import AddMedicationForm from "./components/AddMedicationForm";
import UpdateMedicationForm from "./components/UpdateMedicationForm";
import DeleteMedicationForm from "./components/DeleteMedicationForm";
import AddMedicationPlanForm from "./components/AddMedicationPlanForm";
import UpdateMedicationPlanForm from "./components/UpdateMedicationPlanForm";
import AssignMedicationForm from "./components/AssignMedicationForm";
import RemoveMedicationForm from "./components/RemoveMedicationForm";
import AssignPatientToCaregiver from "./components/AssignPatientToCaregiver";
import RemovePatientFromCaregiver from "./components/RemovePatientFromCaregiver";
import AddPlanToPatient from "./components/AddPlanToPatient";
import RemovePlanFromPatient from "./components/RemovePlanFromPatient";

class MedicContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleAddPatientForm = this.toggleAddPatientForm.bind(this);
        this.toggleAddMedicationForm = this.toggleAddMedicationForm.bind(this);
        this.toggleAddMedicationPlanForm = this.toggleAddMedicationPlanForm.bind(this);
        this.toggleUpdateMedicationPlanForm = this.toggleUpdateMedicationPlanForm.bind(this);
        this.toggleDeletePatientForm = this.toggleDeletePatientForm.bind(this);
        this.toggleRemoveMedicationFromPlanForm = this.toggleRemoveMedicationFromPlanForm.bind(this);
        this.toggleUpdatePatientForm = this.toggleUpdatePatientForm.bind(this);
        this.toggleUpdateMedicationForm = this.toggleUpdateMedicationForm.bind(this);
        this.toggleDeleteMedicationForm = this.toggleDeleteMedicationForm.bind(this);
        this.toggleAddMedicationToPlanForm = this.toggleAddMedicationToPlanForm.bind(this);
        this.toggleAddPatientToCaregiver = this.toggleAddPatientToCaregiver.bind(this);
        this.toggleRemovePatientFromCaregiver = this.toggleRemovePatientFromCaregiver.bind(this);
        this.toggleAddPlanToPatient = this.toggleAddPlanToPatient.bind(this);
        this.toggleRemovePlanFromPatient = this.toggleRemovePlanFromPatient.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            addUserSelected: false,
            updateUserSelected: false,
            deleteUserSelected: false,
            addMedicationSelected: false,
            updateMedicationSelected: false,
            deleteMedicationSelected: false,
            addMedicationPlanSelected: false,
            updateMedicationPlanSelected: false,
            addMedicationToPlanSelected: false,
            removeMedicationFromPlanSelected: false,
            addPatientToCaregiverSelected: false,
            removePatientFromCaregiverSelected: false,
            addPlanToPatientSelected: false,
            removePlanFromPatientSelected: false,
            collapseForm: false,
            patients: [],
            caregivers: [],
            medications: [],
            medicationPlans: [],
            MedicationTableData: [],
            CaregiverTableData: [],
            PatientTableData: [],
            MedicationPlanTableData: [],
            medicationLoaded: false,
            caregiversLoaded: false,
            patientsLoaded: false,
            medicationPlanLoaded: false,
            errorStatus: 0,
            error: null,
            user: this.props.userState
        };
        console.log("User name is ");
        console.log(this.props.userState);
    }

    componentDidMount() {
        this.fetchMedication();
        this.fetchCaregivers();
        this.fetchPatients();
        this.fetchMedicationPlan()

    }

    fetchMedication() {
        return API_MEDICATION.getMedication((result, status, err) => {

            if (result !== null) {
                console.log(result);
                this.setState({
                    MedicationTableData: result,
                    medications: result,
                    medicationLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchCaregivers() {
        let caregiverArray = [];

        return API_CAREGIVER.getCaregivers((result, status, err) => {
            if (result !== null) {
                for (let i = 0; i < result.length; i++) {
                    let PatientNames = "";
                    for (let j = 0; j < result[i]["patients"].length; j++) {
                        PatientNames += result[i]["patients"][j]["user"]["firstName"] + " " + result[i]["patients"][j]["user"]["lastName"] + ", ";
                    }
                    let caregiver = {
                        id: result[i]["id"],
                        firstName: result[i]["user"]["firstName"],
                        lastName: result[i]["user"]["lastName"],
                        adress: result[i]["user"]["adress"],
                        email: result[i]["user"]["email"],
                        gender: result[i]["user"]["gender"],
                        patientNames: PatientNames
                    }
                    caregiverArray[i] = caregiver;
                }
                console.log("Caregivers:");
                console.log(caregiverArray);
                this.setState({
                    caregivers: result,
                    CaregiverTableData: caregiverArray,
                    caregiversLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchPatients() {
        let patientArray = [];
        return API_PATIENT.getPatients((result, status, err) => {
            if (result !== null) {
                console.log(result);
                for (let i = 0; i < result.length; i++) {
                    let MedicationPlans = "";
                    for (let j = 0; j < result[i]["medicationPlans"].length; j++) {
                        MedicationPlans += result[i]["medicationPlans"][j]["id"] + " ";
                    }
                    let patient = {
                        id: result[i]["id"],
                        patientFirstName: result[i]["user"]["firstName"],
                        patientLastName: result[i]["user"]["lastName"],
                        adress: result[i]["user"]["adress"],
                        email: result[i]["user"]["email"],
                        gender: result[i]["user"]["gender"],
                        medicationPlans: MedicationPlans,
                        medicalRecord: result[i]["medicalRecord"]
                    }
                    patientArray[i] = patient;
                }
                console.log("Modification result: ")
                console.log(patientArray);
                this.setState({
                    patients: result,
                    PatientTableData: patientArray,
                    patientsLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    fetchMedicationPlan() {
        return API_MEDICATIONPLAN.getMedicationPlans((result, status, err) => {
            let medicationPlansArray = [];
            if (result !== null) {
                console.log(result);

                for (let i = 0; i < result.length; i++) {
                    let medications = "";
                    for (let j = 0; j < result[i]["medications"].length; j++) {
                        medications += result[i]["medications"][j]["id"] + " ";
                    }
                    let medicationPlan = {
                        id: result[i]["id"],
                        startTime: result[i]["startTime"].split(" ")[1],
                        endTime: result[i]["endTime"].split(" ")[1],
                        startDate: result[i]["startDate"],
                        endDate: result[i]["endDate"],
                        medications: medications
                    }
                    medicationPlansArray[i] = medicationPlan;
                }
                console.log("MedicationPlans:");
                console.log(medicationPlansArray);
                this.setState({
                    MedicationPlanTableData: medicationPlansArray,
                    medicationPlans: result,
                    medicationPlanLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });


    }

    toggleAddPatientForm() {
        this.setState({addUserSelected: !this.state.addUserSelected});
    }

    toggleUpdatePatientForm() {
        this.setState({updateUserSelected: !this.state.updateUserSelected});
    }

    toggleDeletePatientForm() {
        this.setState({deleteUserSelected: !this.state.deleteUserSelected});
    }

    toggleAddMedicationForm() {
        this.setState({addMedicationSelected: !this.state.addMedicationSelected});
    }

    toggleUpdateMedicationForm() {
        this.setState({updateMedicationSelected: !this.state.updateMedicationSelected});
    }

    toggleDeleteMedicationForm() {
        this.setState({deleteMedicationSelected: !this.state.deleteMedicationSelected});
    }

    toggleAddMedicationPlanForm() {
        this.setState({addMedicationPlanSelected: !this.state.addMedicationPlanSelected});
    }

    toggleUpdateMedicationPlanForm() {
        this.setState({updateMedicationPlanSelected: !this.state.updateMedicationPlanSelected});
    }

    toggleAddMedicationToPlanForm() {
        this.setState({addMedicationToPlanSelected: !this.state.addMedicationToPlanSelected});
    }

    toggleRemoveMedicationFromPlanForm() {
        this.setState({removeMedicationFromPlanSelected: !this.state.removeMedicationFromPlanSelected});
    }

    toggleAddPatientToCaregiver() {
        this.setState({addPatientToCaregiverSelected: !this.state.addPatientToCaregiverSelected});
    }

    toggleRemovePatientFromCaregiver() {
        this.setState({removePatientFromCaregiverSelected: !this.state.removePatientFromCaregiverSelected});
    }

    toggleAddPlanToPatient() {
        this.setState({addPlanToPatientSelected: !this.state.addPlanToPatientSelected});
    }

    toggleRemovePlanFromPatient() {
        this.setState({removePlanFromPatientSelected: !this.state.removePlanFromPatientSelected});
    }

    reload() {
        this.setState({
            medicationLoaded: false,
            caregiversLoaded: false,
            patientsLoaded: false,
            medicationPlanLoaded: false
        });

        this.fetchCaregivers();
        this.fetchMedication();
        this.fetchMedicationPlan();
        this.fetchPatients();
    }


    render() {

        return (
            <div>

                <CardHeader>
                    <strong> Hello Medic </strong>

                </CardHeader>

                <Card>
                    <h3>Caregivers Table</h3>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            <Button color="primary" onClick={this.toggleAddPatientForm}>Add User</Button>
                            <Button color="primary" onClick={this.toggleDeletePatientForm}>Delete User</Button>
                            <Button color="primary" onClick={this.toggleUpdatePatientForm}>Update User</Button>
                        </Col>
                        <Col sm={{size: '10', offset: 1}}>
                            <Button color="primary" onClick={this.toggleAddPatientToCaregiver}>Add Patient To
                                Caregiver</Button>
                            <Button color="primary" onClick={this.toggleRemovePatientFromCaregiver}>Remove Patient From
                                Caregiver</Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={{size: '8', offset: 1}} className="caregiverTable">
                            {this.state.caregiversLoaded && <CaregiverTable tableData={this.state.CaregiverTableData}/>}
                        </Col>
                    </Row>
                </Card>

                <Card>
                    <h3>PatientTable</h3>
                    <br/>
                    <Row>
                        <Col>
                            <Button color="primary" onClick={this.toggleAddPlanToPatient}>Add Plan to Patient</Button>
                            <Button color="primary" onClick={this.toggleRemovePlanFromPatient}>Remove Plan From
                                Patient</Button>
                        </Col>
                        <Col sm={{size: '8', offset: 1}} className="patientTable">
                            {this.state.patientsLoaded && <PatientTable tableData={this.state.PatientTableData}/>}
                        </Col>
                    </Row>
                </Card>
                <Card>
                    <h3>Medication Table</h3>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            <Button color="primary" onClick={this.toggleAddMedicationForm}>Add Medication</Button>
                            <Button color="primary" onClick={this.toggleUpdateMedicationForm}>Update Medication</Button>
                            <Button color="primary" onClick={this.toggleDeleteMedicationForm}>Delete Medication</Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={{size: '8', offset: 1}} className="medicationTable">
                            {this.state.medicationLoaded &&
                            <MedicationTable tableData={this.state.MedicationTableData}/>}
                        </Col>
                    </Row>
                </Card>
                <Card>
                    <h3>MedicationPlanTable</h3>
                    <br/>
                    <Row>
                        <Col>
                            <Button color="primary" onClick={this.toggleAddMedicationPlanForm}>Add Medication
                                Plan</Button>
                            <Button color="primary" onClick={this.toggleUpdateMedicationPlanForm}>Update Medication
                                Plan</Button>
                            <Button color="primary" onClick={this.toggleAddMedicationToPlanForm}>Add Medication to
                                Plan</Button>
                            <Button color="primary" onClick={this.toggleRemoveMedicationFromPlanForm}>Remove Medication
                                From Plan</Button>
                        </Col>
                        <Col sm={{size: '8', offset: 1}} className="medicationPlanTable">
                            {this.state.medicationPlanLoaded &&
                            <MedicationPlanTable tableData={this.state.MedicationPlanTableData}/>}
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.addUserSelected} toggle={this.toggleAddPatientForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddPatientForm}> Add User: </ModalHeader>
                    <ModalBody>
                        <PersonForm reloadHandlerAdd={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.updateUserSelected} toggle={this.toggleUpdatePatientForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdatePatientForm}> Update User: </ModalHeader>
                    <ModalBody>
                        <UpdateUserForm reloadHandlerUpdate={this.reload} patients={this.state.patients}
                                        caregivers={this.state.caregivers}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.deleteUserSelected} toggle={this.toggleDeletePatientForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleDeletePatientForm}> Delete User: </ModalHeader>
                    <ModalBody>
                        <DeleteUserForm reloadHandlerDelete={this.reload} patients={this.state.patients}
                                        caregivers={this.state.caregivers}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.addMedicationSelected} toggle={this.toggleAddMedicationForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddMedicationForm}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <AddMedicationForm reloadHandlerAddMedication={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.updateMedicationSelected} toggle={this.toggleUpdateMedicationForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateMedicationForm}> Update Medication: </ModalHeader>
                    <ModalBody>
                        <UpdateMedicationForm reloadHandlerUpdate={this.reload} medications={this.state.medications}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.deleteMedicationSelected} toggle={this.toggleDeleteMedicationForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleDeleteMedicationForm}> Delete Medication: </ModalHeader>
                    <ModalBody>
                        <DeleteMedicationForm reloadHandlerDeleteMedication={this.reload}
                                              medications={this.state.medications}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.addMedicationPlanSelected} toggle={this.toggleAddMedicationPlanForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddMedicationPlanForm}> Add Medication Plan: </ModalHeader>
                    <ModalBody>
                        <AddMedicationPlanForm reloadHandlerAddMedicationPlan={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.updateMedicationPlanSelected} toggle={this.toggleUpdateMedicationPlanForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateMedicationPlanForm}> Add Medication Plan: </ModalHeader>
                    <ModalBody>
                        <UpdateMedicationPlanForm reloadHandlerUpdateMedicationPlan={this.reload}
                                                  medicationPlans={this.state.medicationPlans}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.addMedicationToPlanSelected} toggle={this.toggleAddMedicationToPlanForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddMedicationToPlanForm}> Add Medication to Plan: </ModalHeader>
                    <ModalBody>
                        <AssignMedicationForm reloadHandlerAddMedicationToPlan={this.reload}
                                              medicationPlans={this.state.medicationPlans}
                                              medications={this.state.medications}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.removeMedicationFromPlanSelected}
                       toggle={this.toggleRemoveMedicationFromPlanForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleRemoveMedicationFromPlanForm}> Add Medication to
                        Plan: </ModalHeader>
                    <ModalBody>
                        <RemoveMedicationForm reloadHandlerRemoveMedicationFromPlan={this.reload}
                                              medicationPlans={this.state.medicationPlans}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.addPatientToCaregiverSelected} toggle={this.toggleAddPatientToCaregiver}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddPatientToCaregiver}> Add Patient to Caregiver: </ModalHeader>
                    <ModalBody>
                        <AssignPatientToCaregiver reloadHandlerAddPatientToCaregiver={this.reload}
                                                  caregivers={this.state.caregivers} patients={this.state.patients}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.removePatientFromCaregiverSelected}
                       toggle={this.toggleRemovePatientFromCaregiver}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleRemovePatientFromCaregiver}> Remove Patient From
                        Caregiver: </ModalHeader>
                    <ModalBody>
                        <RemovePatientFromCaregiver reloadHandlerRemovePatientFromCaregiver={this.reload}
                                                    caregivers={this.state.caregivers}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.addPlanToPatientSelected} toggle={this.toggleAddPlanToPatient}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddPlanToPatient}> Add Plan To Patient: </ModalHeader>
                    <ModalBody>
                        <AddPlanToPatient addPlanToPatientHandler={this.reload} patients={this.state.patients}
                                          medicationPlans={this.state.medicationPlans}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.removePlanFromPatientSelected} toggle={this.toggleRemovePlanFromPatient}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleRemovePlanFromPatient}> Remove Plan From Patient: </ModalHeader>
                    <ModalBody>
                        <RemovePlanFromPatient removePlanFromPatientHandler={this.reload}
                                               patients={this.state.patients}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}

export default MedicContainer;