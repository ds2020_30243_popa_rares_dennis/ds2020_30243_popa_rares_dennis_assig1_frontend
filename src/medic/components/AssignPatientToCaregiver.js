import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Row} from "reactstrap";
import * as API_CAREGIVER from "../api/caregiver-api"

class AssignPatientToCaregiver extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandlerAddPatientToCaregiver;
        this.patients = this.props.patients;
        this.caregivers = this.props.caregivers;

        let patientIdAux = (this.patients.length > 0) ? this.patients[0]["id"]:"";
        let caregiverIdAux = (this.caregivers.length > 0) ? this.caregivers[0]["id"]:"";

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: (patientIdAux !== "" && caregiverIdAux !== ""),

            patientId: patientIdAux,
            caregiverId: caregiverIdAux,

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    findPatientId(email){
        for (let i = 0; i < this.patients.length; i++) {
            if(email === this.patients[i]["user"]["email"]){
                return this.patients[i]["id"];
            }
        }
        return -1;
    }

    findCaregiverId(email){
        for (let i = 0; i < this.caregivers.length; i++) {
            if(email === this.caregivers[i]["user"]["email"]){
                return this.caregivers[i]["id"];
            }
        }
        return -1;
    }

    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;
        let auxState = this.state;
        if(name === "caregiverEmail"){
            auxState.caregiverId = this.findCaregiverId(value);
        }
        if(name === "patientEmail"){
            auxState.patientId = this.findPatientId(value);
        }

        if(this.state.caregiverId !== "" && this.state.patientId !== ""){
            auxState.formIsValid = true;
        }
        this.setState(auxState);

    };

    assignPatientToCaregiver(caregiverId,patientId) {
        return API_CAREGIVER.addPatientToCaregiver(caregiverId,patientId, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        console.log("Call is: "+this.state.caregiverId+" "+this.state.patientId);
        this.assignPatientToCaregiver(this.state.caregiverId,this.state.patientId);
    }

    createSelectItemsCaregivers() {

        let items = [];
        for (let i = 0; i < this.caregivers.length; i++) {
            items.push(<option
                value={this.caregivers[i]["user"]["email"]}>{this.caregivers[i]["user"]["email"]}</option>);
        }
        return items;
    }

    createSelectItemsPatients() {

        let items = [];
        for (let i = 0; i < this.patients.length; i++) {
            items.push(<option
                value={this.patients[i]["user"]["email"]}>{this.patients[i]["user"]["email"]}</option>);
        }

        return items;
    }

    render() {
        return (
            <div>

                <FormGroup id='caregiverEmail'>
                    <label htmlFor='caregiverEmailField'>Caregiver Email: </label>
                    <select name="caregiverEmail" id="caregiverEmailField" onChange={this.handleChange}>
                        {this.createSelectItemsCaregivers()}
                    </select>
                </FormGroup>

                <FormGroup id='patientEmail'>
                    <label htmlFor='patientEmailField'>Patient Email: </label>
                    <select name="patientEmail" id="patientEmailField" onChange={this.handleChange}>
                        {this.createSelectItemsPatients()}
                    </select>
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default AssignPatientToCaregiver;
