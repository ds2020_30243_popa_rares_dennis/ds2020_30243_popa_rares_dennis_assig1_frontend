import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import * as API_MEDICATION from "../api/medication-api"

class AddMedicationForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandlerAddMedication;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            formControls: {
                name: {
                    value: '',
                    placeholder: 'Drug Name ...',
                    valid: false,
                    touched: false,
                },
                dosage: {
                    value: '',
                    placeholder: 'Dosage ...',
                    valid: false,
                    touched: false,
                },
                sideEffects: {
                    value: '',
                    placeholder: 'Side effects...',
                    valid: false,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    check_form(name, value) {
        if (name === "name" && value !== "")
            return true;
        else {
            if (name === "dosage") {
                return /^-?[0-9]+(e[0-9]+)?(\.[0-9]+)?$/.test(value);
            } else {
                if (name === "sideEffects" && value !== "") {
                    return true;
                }
            }
        }
        return false;
    }

    checkAllFields() {
        return this.state.formControls.name.valid && this.state.formControls.dosage.valid && this.state.formControls.sideEffects.valid;
    }

    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;
        let check_value = this.check_form(name, value);
        console.log(name + " " + value + " " + check_value);
        let auxState = this.state.formControls;
        switch (name) {
            case "name": {auxState.name.value=value;auxState.name.touched=true;auxState.name.valid=check_value;break;}
            case "dosage": {auxState.dosage.value=value;auxState.dosage.touched=true;auxState.dosage.valid=check_value;break;}
            case "sideEffects": {auxState.sideEffects.value=value;auxState.sideEffects.touched=true;auxState.sideEffects.valid=check_value;break;}
            default:
                break;
        }

        let validation = this.checkAllFields();

        this.setState({formControls:auxState,formIsValid:validation});

        console.log(this.state.formControls);

    };

    registerMedication(medication) {
        return API_MEDICATION.postMedication(medication, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let medication = {
            name:this.state.formControls.name.value,
            dosage:this.state.formControls.dosage.value,
            sideEffects: [this.state.formControls.sideEffects.value],
        }
        this.registerMedication(medication);

    }

    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField'
                           placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched ? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> *Name can't be empty </div>}
                </FormGroup>

                <FormGroup id='dosage'>
                    <Label for='dosageField'> Dosage: </Label>
                    <Input name='dosage' id='dosageField' placeholder={this.state.formControls.dosage.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.dosage.value}
                           touched={this.state.formControls.dosage.touched ? 1 : 0}
                           valid={this.state.formControls.dosage.valid}
                           required
                    />
                    {this.state.formControls.dosage.touched && !this.state.formControls.dosage.valid &&
                    <div className={"error-message row"}> *Dosage must be a real number! </div>}
                </FormGroup>

                <FormGroup id='sideEffects'>
                    <Label for='sideEffectsField'> Side effects: </Label>
                    <Input name='sideEffects' id='sideEffectsField'
                           placeholder={this.state.formControls.sideEffects.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.sideEffects.value}
                           touched={this.state.formControls.sideEffects.touched ? 1 : 0}
                           valid={this.state.formControls.sideEffects.valid}
                           required
                    />
                    {this.state.formControls.sideEffects.touched && !this.state.formControls.sideEffects.valid &&
                    <div className={"error-message row"}> *Side effects must not be empty! </div>}
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default AddMedicationForm;
