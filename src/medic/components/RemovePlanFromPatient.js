import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Row} from "reactstrap";
import * as API_PATIENT from "../api/patient-api"

class RemovePlanFromPatient extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.removePlanFromPatientHandler;
        this.patients = this.props.patients;
        this.patientEmailAux = this.patients.length > 0 ? this.patients[0]["user"]["email"] : "";
        this.planIdAux = (this.patients.length > 0) ? (this.patients[0]["medicationPlans"].length > 0 ? this.patients[0]["medicationPlans"][0]["id"] : "") : "";
        this.state = {
            errorStatus: 0,
            error: null,
            planId: this.planIdAux,
            patientEmail: this.patientEmailAux,
            formIsValid: (this.patientEmailAux !== "" && this.planIdAux !== "") ? true : false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    findPatientId(email) {
        for (let i = 0; i < this.patients.length; i++) {

            if (this.patients[i]["user"]["email"] === email) {
                return this.patients[i]["id"];
            }

        }
        return "";
    }

    findFirstPlanId(email) {
        for (let i = 0; i < this.patients.length; i++) {
            for (let j = 0; j < this.patients[i]["medicationPlans"].length; j++) {
                if (this.patients[i]["user"]["email"] === email) {
                    return this.patients[i]["medicationPlans"][j]["id"];
                }
            }
        }
        return "";
    }

    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;
        let auxState = this.state;

        if (name === "patientEmail") {
            auxState.patientEmail = value;
            auxState.planId = this.findFirstPlanId(value);
        }
        if (name === "medicationPlanId") {
            auxState.planId = value;
        }

        if (auxState.patientEmail !== "" && auxState.planId !== "") {
            auxState.formIsValid = true;
        } else {
            auxState.formIsValid = false;
        }

        this.setState(auxState);
    };

    removePlanFromPatient(patientId, planId) {
        return API_PATIENT.removePlanFromPatient(patientId, planId, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        console.log(this.state);
        let planId = this.state.planId;
        let patientId = this.findPatientId(this.state.patientEmail);
        console.log("remove call " + patientId + " " + planId);
        this.removePlanFromPatient(patientId, planId);
    }

    createSelectItemsMedicationPlan(email) {
        let items = [];
        for (let i = 0; i < this.patients.length; i++) {
            if (this.patients[i]["user"]["email"] === email) {
                for (let j = 0; j < this.patients[i]["medicationPlans"].length; j++) {
                    items.push(<option
                        value={this.patients[i]["medicationPlans"][j]["id"]}>{this.patients[i]["medicationPlans"][j]["id"]}</option>);
                }
            }
        }

        return items;
    }

    createSelectItemsPatients() {
        let items = [];
        for (let i = 0; i < this.patients.length; i++) {
            items.push(<option
                value={this.patients[i]["user"]["email"]}>{this.patients[i]["user"]["email"]}</option>);
        }
        return items;
    }

    render() {
        return (
            <div>
                <FormGroup id='patientEmail'>
                    <label htmlFor='patientEmailField'> Patient Email: </label>
                    <select name="patientEmail" id="patientEmailField" onChange={this.handleChange}>
                        {this.createSelectItemsPatients()}
                    </select>
                </FormGroup>

                <FormGroup id='medicationPlanId'>
                    <label htmlFor='medicationPlanIdField'> Plan Id: </label>
                    <select name="medicationPlanId" id="medicationPlanIdField" onChange={this.handleChange}>
                        {this.createSelectItemsMedicationPlan(this.state.patientEmail)}
                    </select>
                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default RemovePlanFromPatient;
