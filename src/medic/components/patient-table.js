import React from "react";
import Table from "../../commons/tables/table";

const columns = [
    {
        Header: 'Id',
        accessor: 'id',
    },
    {
        Header: 'Medical Record',
        accessor: 'medicalRecord',
    },
    {
        Header: 'Patient First Name',
        accessor: 'patientFirstName',
    },
    {
        Header: 'Patient Last Name',
        accessor: 'patientLastName',
    },
    {
        Header: 'Address',
        accessor: 'adress',
    },
    {
        Header: 'Email',
        accessor: 'email',
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Medication Plans',
        accessor: 'medicationPlans',
    },

];

const filters = [
    {
        accessor: 'patientFirstName'
    }
];

class PatientTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }

}

export default PatientTable;