import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Row} from "reactstrap";
import * as API_MEDICATIONPLAN from "../api/medicationPlan-api"

class AssignMedicationForm extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandlerAddMedicationToPlan;
        this.medications = this.props.medications;
        this.medicationPlans = this.props.medicationPlans;

        let medicationPlanIdAux = (this.medicationPlans.length > 0) ? this.medicationPlans[0]["id"] : "";
        let medicationIdAux = (this.medications.length > 0) ? this.medications[0]["id"] : "";

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: (medicationIdAux !== "" && medicationPlanIdAux !== ""),

            medicationId: medicationIdAux,
            medicationPlanId:medicationPlanIdAux

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    findMedicationId(name){
        for (let i = 0; i < this.medications.length; i++) {
            if(name === this.medications[i]["name"]){
                return this.medications[i]["id"];
            }
        }
        return -1;
    }

    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;

        if(name === "medicationPlanId"){
            this.setState({medicationPlanId:value});
        }
        if(name === "medicationName"){
            this.setState({medicationId:this.findMedicationId(value)});
        }

        if(this.state.medicationId !== "" && this.state.medicationPlanId !== ""){
            this.setState({formIsValid:true});
        }

        console.log(name + " " + value + " " );
    };

    assignMedicationToPlan(medicationPlanId,medicationId) {
        return API_MEDICATIONPLAN.addMedicationToPlan(medicationPlanId,medicationId, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        this.assignMedicationToPlan(this.state.medicationPlanId,this.state.medicationId);
    }

    createSelectItemsMedicationPlans() {

        let items = [];
        for (let i = 0; i < this.medicationPlans.length; i++) {
            items.push(<option
                value={this.medicationPlans[i]["id"]}>{this.medicationPlans[i]["id"]}</option>);
        }

        return items;
    }

    createSelectItemsMedications() {

        let items = [];
        for (let i = 0; i < this.medications.length; i++) {
            items.push(<option
                value={this.medications[i]["name"]}>{this.medications[i]["name"]}</option>);
        }

        return items;
    }

    render() {
        return (
            <div>

                <FormGroup id='medicationPlanId'>
                    <label htmlFor='medicationPlanIdField'> Medication Plan Id: </label>
                    <select name="medicationPlanId" id="medicationPlanIdField" onChange={this.handleChange}>
                        {this.createSelectItemsMedicationPlans()}
                    </select>
                </FormGroup>

                <FormGroup id='medicationName'>
                    <label htmlFor='medicationNameField'> Medication Name: </label>
                    <select name="medicationName" id="medicationNameField" onChange={this.handleChange}>
                        {this.createSelectItemsMedications()}
                    </select>
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default AssignMedicationForm;
