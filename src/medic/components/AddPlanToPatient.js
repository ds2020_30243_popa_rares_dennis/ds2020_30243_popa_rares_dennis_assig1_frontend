import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Row} from "reactstrap";
import * as API_PATIENT from "../api/patient-api"

class AddPlanToPatient extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.addPlanToPatientHandler;
        this.patients = this.props.patients;
        this.medicationPlans = this.props.medicationPlans;

        this.patientEmailAux = (this.patients.length > 0) ? this.patients[0]["user"]["email"] : "";
        this.medicationPlanIdAux = (this.medicationPlans.length > 0) ? this.medicationPlans[0]["id"] : "";

        this.state = {
            errorStatus: 0,
            error: null,
            patientEmail: this.patientEmailAux,
            medicationPlanId: this.medicationPlanIdAux,
            formIsValid: (this.patientEmailAux !== "" && this.medicationPlanIdAux !== "") ? true : false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    findPatientId(email) {
        for (let i = 0; i < this.patients.length; i++) {
            if (this.patients[i]["user"]["email"] === email) {
                return this.patients[i]["id"];
            }
        }
        return "";
    }


    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;
        let auxState = this.state;
        if (name === "medicationPlanId") {
            auxState.medicationPlanId = value;
        }
        if (name === "patientEmail") {
            auxState.patientEmail = value;
        }

        if (auxState.patientEmail !== "" && auxState.medicationPlanId !== "") {
            auxState.formIsValid = true;
        } else {
            auxState.formIsValid = false;
        }

        this.setState(auxState);
    };

    addPlanToPatient(patientId, planId) {
        return API_PATIENT.addPlanToPatient(patientId, planId, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        console.log(this.state);
        let planId = this.state.medicationPlanId;
        let patientId = this.findPatientId(this.state.patientEmail);
        console.log("add call " + patientId + " " + planId);
        this.addPlanToPatient(patientId,planId );
    }

    createSelectItemsPlans() {
        let items = [];
        for (let i = 0; i < this.medicationPlans.length; i++) {
            items.push(<option
                value={this.medicationPlans[i]["id"]}>{this.medicationPlans[i]["id"]}</option>);
        }
        return items;
    }

    createSelectItemsPatients() {
        let items = [];
        for (let i = 0; i < this.patients.length; i++) {
            items.push(<option
                value={this.patients[i]["user"]["email"]}>{this.patients[i]["user"]["email"]}</option>);
        }
        return items;
    }

    render() {
        return (
            <div>

                <FormGroup id='patientEmail'>
                    <label htmlFor='patientEmailField'> Patient Email: </label>
                    <select name="patientEmail" id="patientEmailField" onChange={this.handleChange}>
                        {this.createSelectItemsPatients()}
                    </select>
                </FormGroup>

                <FormGroup id='medicationPlanId'>
                    <label htmlFor='medicationPlanIdField'>Plan Id: </label>
                    <select name="medicationPlanId" id="medicationPlanIdField" onChange={this.handleChange}>
                        {this.createSelectItemsPlans()}
                    </select>
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default AddPlanToPatient;
