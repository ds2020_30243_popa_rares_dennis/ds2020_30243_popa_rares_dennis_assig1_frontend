import React from "react";
import Table from "../../commons/tables/table";

const columns = [
    {
        Header: 'Id',
        accessor: 'id',
    },
    {
        Header: 'FROM',
        accessor: 'startTime',
    },
    {
        Header: 'TO',
        accessor: 'endTime',
    },
    {
        Header: 'Start Date',
        accessor: 'startDate',
    } ,
    {
        Header: 'End Date',
        accessor: 'endDate',
    },
    {
        Header: 'Medications Id',
        accessor: 'medications',
    }
];

const filters = [
    {
        accessor: 'id',
    }
];

class MedicationPlanTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }

}

export default MedicationPlanTable;