import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Row} from "reactstrap";
import * as API_MEDICATIONPLAN from "../api/medicationPlan-api"

class RemoveMedicationForm extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandlerRemoveMedicationFromPlan;
        this.medicationPlans = this.props.medicationPlans;
        this.createSelectItemsMedicationPlans = this.createSelectItemsMedicationPlans.bind(this);
        this.createSelectItemsMedications = this.createSelectItemsMedications.bind(this);

        let medicationPlanIdAux = (this.medicationPlans.length > 0) ? this.medicationPlans[0]["id"] : "-1";
        let medicationIdAux = (medicationPlanIdAux !== "-1" && this.medicationPlans[0]["medications"].length > 0) ? this.medicationPlans[0]["medications"][0]["id"] : "";

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: (medicationPlanIdAux !== "-1" && medicationIdAux !== ""),

            medicationId: medicationIdAux,
            medicationPlanId: medicationPlanIdAux,
            isPlanIdSet: false

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    findMedicationId(name) {
        for (let i = 0; i < this.medicationPlans.length; i++) {
            if (this.medicationPlans[i]["id"] === parseInt(this.state.medicationPlanId)) {
                for (let j = 0; j < this.medicationPlans[i]["medications"].length; j++) {
                    if (this.medicationPlans[i]["medications"][j]["name"] === name) {
                        return this.medicationPlans[i]["medications"][j]["id"];
                    }
                }
            }
        }
        return -1;
    }

    findFirstMedicationId(planId) {
        for (let i = 0; i < this.medicationPlans.length; i++) {
            if (this.medicationPlans[i]["id"] === parseInt(planId)) {
                for (let j = 0; j < this.medicationPlans[i]["medications"].length; j++) {
                    return this.medicationPlans[i]["medications"][j]["id"];
                }
            }
        }
        return "";
    }

    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;

        if (name === "medicationPlanId") {
            this.setState({medicationPlanId: value, isPlanIdSet: true,medicationId:this.findFirstMedicationId(value)});
            console.log(this.medicationPlans);
        }
        if (name === "medicationName") {
            this.setState({medicationId: this.findMedicationId(value)});
        }

        if (this.state.medicationId !== "" && this.state.medicationPlanId !== "") {
            this.setState({formIsValid: true});
        }

        console.log(name + " " + value + " ");
    };

    removeMedicationFromPlan(medicationPlanId, medicationId) {
        return API_MEDICATIONPLAN.removeMedicationFromPlan(medicationPlanId, medicationId, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        console.log(this.state);
        this.removeMedicationFromPlan(this.state.medicationPlanId, this.state.medicationId);
    }

    createSelectItemsMedicationPlans() {
        console.log("1");
        console.log(this.medicationPlans);
        let items = [];
        for (let i = 0; i < this.medicationPlans.length; i++) {
            items.push(<option
                value={this.medicationPlans[i]["id"]}>{this.medicationPlans[i]["id"]}</option>);
        }

        return items;
    }

    createSelectItemsMedications() {
        let items = [];
        for (let i = 0; i < this.medicationPlans.length; i++) {
            if (this.medicationPlans[i]["id"] === parseInt(this.state.medicationPlanId)) {
                for (let j = 0; j < this.medicationPlans[i]["medications"].length; j++) {
                    items.push(<option
                        value={this.medicationPlans[i]["medications"][j]["name"]}>{this.medicationPlans[i]["medications"][j]["name"]}</option>);
                }
            }
        }
        return items;
    }

    render() {
        return (
            <div>

                <FormGroup id='medicationPlanId'>
                    <label htmlFor='medicationPlanIdField'> Medication Plan Id: </label>
                    <select name="medicationPlanId" id="medicationPlanIdField" onChange={this.handleChange}>
                        {this.createSelectItemsMedicationPlans()}
                    </select>
                </FormGroup>

                <FormGroup id='medicationName'>
                    <label htmlFor='medicationNameField'> Medication Name: </label>
                    <select name="medicationName" id="medicationNameField" onChange={this.handleChange}>
                        {this.createSelectItemsMedications(this.state.medicationPlanId)}
                    </select>
                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default RemoveMedicationForm;
