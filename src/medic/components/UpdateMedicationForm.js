import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import * as API_MEDICATION from "../api/medication-api"


class UpdateMedicationForm extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandlerUpdate;
        this.medications = this.props.medications;
        let medicationNameAux = (this.medications.length > 0) ? this.medications[0]["name"] : "";
        let medicationIdAux = medicationNameAux !== "" ? this.findMedicationId(medicationNameAux):"";

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            medicationName: medicationNameAux,
            medicationId: medicationIdAux,
            formControls: {
                name: {
                    value: '',
                    placeholder: 'Drug Name ...',
                    valid: false,
                    touched: false,
                },
                dosage: {
                    value: '',
                    placeholder: 'Dosage ...',
                    valid: false,
                    touched: false,
                },
                sideEffects: {
                    value: '',
                    placeholder: 'Side effects...',
                    valid: false,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    check_form(name, value) {
        if (name === "name" && value !== "")
            return true;
        else {
            if (name === "dosage") {
                return /^-?[0-9]+(e[0-9]+)?(\.[0-9]+)?$/.test(value);
            } else {
                if (name === "sideEffects" && value !== "") {
                    return true;
                }
            }
        }
        return false;
    }

    checkAllFields() {
        return this.state.formControls.name.valid && this.state.formControls.dosage.valid && this.state.formControls.sideEffects.valid;
    }

    findMedicationId(name){
        for (let i = 0; i < this.medications.length; i++) {
            if(name === this.medications[i]["name"]){
                return this.medications[i]["id"];
            }
        }
        return -1;
    }

    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;
        let check_value = this.check_form(name, value);
        console.log(name + " " + value + " " + check_value);
        let auxState = this.state;
        switch (name) {
            case "medicationName":{
                auxState.medicationName = value;
                auxState.medicationId = this.findMedicationId(value);
                break;
            }
            case "name": {
                auxState.formControls.name.value = value;
                auxState.formControls.name.touched = true;
                auxState.formControls.name.valid = check_value;
                break;
            }
            case "dosage": {
                auxState.formControls.dosage.value = value;
                auxState.formControls.dosage.touched = true;
                auxState.formControls.dosage.valid = check_value;
                break;
            }
            case "sideEffects": {
                auxState.formControls.sideEffects.value = value;
                auxState.formControls.sideEffects.touched = true;
                auxState.formControls.sideEffects.valid = check_value;
                break;
            }
            default:
                break;
        }

        let validation = this.checkAllFields();
        auxState.formControls.formIsValid = validation;
        this.setState(auxState);
        this.setState({formIsValid:validation});
        console.log(this.state.formControls);
        console.log(this.state.formIsValid);

    };


    updateMedication(medication) {
        return API_MEDICATION.updateMedication(medication, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let medication = {
            id:this.state.medicationId,
            sideEffects: [this.state.formControls.sideEffects.value],
            dosage: this.state.formControls.dosage.value,
            name: this.state.formControls.name.value
        }

        this.updateMedication(medication);

    }

    createSelectItemsMedications() {

        let items = [];
        for (let i = 0; i < this.medications.length; i++) {
            items.push(<option
                value={this.medications[i]["name"]}>{this.medications[i]["name"]}</option>);
        }

        return items;
    }

    render() {
        return (
            <div>

                <FormGroup id='medicationName'>
                    <label htmlFor='medicationNameField'> Medication Name: </label>
                    <select name="medicationName" id="medicationNameField" onChange={this.handleChange}>
                        {this.createSelectItemsMedications()}
                    </select>
                </FormGroup>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField'
                           placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched ? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> *Name can't be empty </div>}
                </FormGroup>

                <FormGroup id='dosage'>
                    <Label for='dosageField'> Dosage: </Label>
                    <Input name='dosage' id='dosageField' placeholder={this.state.formControls.dosage.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.dosage.value}
                           touched={this.state.formControls.dosage.touched ? 1 : 0}
                           valid={this.state.formControls.dosage.valid}
                           required
                    />
                    {this.state.formControls.dosage.touched && !this.state.formControls.dosage.valid &&
                    <div className={"error-message row"}> *Dosage must be a real number! </div>}
                </FormGroup>

                <FormGroup id='sideEffects'>
                    <Label for='sideEffectsField'> Side effects: </Label>
                    <Input name='sideEffects' id='sideEffectsField'
                           placeholder={this.state.formControls.sideEffects.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.sideEffects.value}
                           touched={this.state.formControls.sideEffects.touched ? 1 : 0}
                           valid={this.state.formControls.sideEffects.valid}
                           required
                    />
                    {this.state.formControls.sideEffects.touched && !this.state.formControls.sideEffects.valid &&
                    <div className={"error-message row"}> *Side effects must not be empty! </div>}
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default UpdateMedicationForm;
