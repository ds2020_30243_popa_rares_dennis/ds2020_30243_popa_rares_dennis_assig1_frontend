import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Row} from "reactstrap";
import * as API_CAREGIVER from "../api/caregiver-api"
import * as API_PATIENT from "../api/patient-api"

class DeleteUserForm extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandlerDelete;
        this.caregivers = this.props.caregivers;
        this.patients = this.props.patients;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            userType: "PATIENT",
            userEmail: "",
            userId:"",
            roleId:"",

            medicationPlans:[],
            patientsList:[],
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }




    findUserId(email){
        if(this.state.userType === "PATIENT"){
            for (let i = 0; i < this.patients.length; i++) {
                if(this.patients[i]["user"]["email"] === email)
                    return this.patients[i]["user"]["id"];
            }
        }else{
            for (let i = 0; i < this.caregivers.length; i++) {
                if(this.caregivers[i]["user"]["email"] === email)
                    return this.caregivers[i]["user"]["id"];
            }
        }
        return -1;
    }

    findRoleId(email){
        if(this.state.userType === "PATIENT"){
            for (let i = 0; i < this.patients.length; i++) {
                if(this.patients[i]["user"]["email"] === email)
                    return this.patients[i]["id"];
            }
        }else{
            for (let i = 0; i < this.caregivers.length; i++) {
                if(this.caregivers[i]["user"]["email"] === email)
                    return this.caregivers[i]["id"];
            }
        }
        return -1;
    }

    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;
        console.log(name + " " + value + " ");

        switch(name){
            case "userType": this.setState({userType:value});break;
            case "userEmail":{let userId = this.findUserId(value),roleId=this.findRoleId(value);this.setState({userEmail:value,userId:userId,roleId:roleId,formIsValid:true});console.log("User id:" + userId +" roleid:"+roleId);break;}
            default: break;
        }

        console.log(this.state.formControls);

    };

    deletePatient(patient) {
        return API_PATIENT.deletePatient(patient, (result, status, error) => {
            if (result !== null ) {
                console.log("Successfully inserted patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }
    deleteCaregiver(caregiver) {
        return API_CAREGIVER.deleteCaregiver(caregiver, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted caregiver with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {

        let patient,caregiver;
        if(this.state.userType === "PATIENT"){
            patient = this.state.roleId;
            this.deletePatient(patient);
            console.log(patient);
        }else{
            caregiver = this.state.roleId;
            console.log(caregiver);
            this.deleteCaregiver(caregiver);
        }

    }

    createSelectItemsUsers() {

        let items = [];
        if(this.state.userType === "PATIENT") {
            for (let i = 0; i < this.patients.length; i++) {

                items.push(<option
                    value={this.patients[i]["user"]["email"]}>{this.patients[i]["user"]["email"]}</option>);
            }
        }else {
            for (let i = 0; i < this.caregivers.length; i++) {

                items.push(<option
                    value={this.caregivers[i]["user"]["email"]}>{this.caregivers[i]["user"]["email"]}</option>);
            }
        }

        return items;
    }

    render() {
        return (
            <div>


                <FormGroup id='userType'>
                    <label htmlFor='userTypeField'> User type: </label>
                    <select name="userType" id="userTypeField" onChange={this.handleChange}>
                        <option key="PATIENT" value="PATIENT"> PATIENT </option>
                        <option key="CAREGIVER" value="CAREGIVER"> CAREGIVER </option>
                    </select>
                </FormGroup>

                <FormGroup id='userEmail'>
                    <label htmlFor='userEmailField'> User Email: </label>
                    <select name="userEmail" id="userEmailField" onChange={this.handleChange}>
                        {this.createSelectItemsUsers()}
                    </select>
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default DeleteUserForm;
