import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import * as API_CAREGIVER from "../api/caregiver-api"
import * as API_PATIENT from "../api/patient-api"

class UpdateUserForm extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandlerUpdate;
        this.caregivers = this.props.caregivers;
        this.patients = this.props.patients;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            userType: "PATIENT",
            userEmail: "",
            userId:"",
            roleId:"",
            medicationPlans:[],
            patientsList:[],
            formControls: {
                firstName: {
                    value: '',
                    placeholder: 'What is your first name?...',
                    valid: false,
                    touched: false,
                },
                lastName: {
                    value: '',
                    placeholder: 'What is your  last name?...',
                    valid: false,
                    touched: false,
                },
                birthdate: {
                    value: '',
                    valid: false,
                    touched: false,
                },
                gender: {
                    value: 'MALE',
                    valid: true,
                    touched: false,
                },
                adress: {
                    value: '',
                    placeholder: 'What is your address?...',
                    valid: false,
                    touched: false,
                },
                email: {
                    value: '',
                    placeholder: 'Email...',
                    valid: false,
                    touched: false,
                },
                password: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
                passwordConfirm: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
                medicalRecord: {
                    value: '',
                    placeholder: 'Medical Record...',
                    valid: false,
                    touched: false,
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    check_form(name,value){
        switch(name){
            case "userType": return true;
            case "userEmail": return /^\S+@\S+$/.test( value);
            case "firstName": return /^[a-zA-Z ]+$/.test( value);
            case "lastName": return /^[a-zA-Z ]+$/.test( value);
            case "birthdate": return Math.floor((new Date() - new Date(value)) / (1000*60*60*24)/365) > 18 ? true : false;
            case "gender": return true;
            case "address": return (value.length > 2) ? true:false;
            case "email": return /^\S+@\S+$/.test( value);
            case "password": return (this.state.formControls.passwordConfirm.value === value) ? true:false;
            case "passwordConfirm": return (this.state.formControls.password.value === value) ? true:false;
            case "medicalRecord": return (value.length > 0) ? true:false;
            default: return false;
        }
    }

    checkAllFields(){
        let state = this.state.formControls;
        return state.firstName.valid && state.lastName.valid && state.birthdate.valid && state.gender.valid && state.adress.valid && state.email.valid && state.password.valid && state.passwordConfirm.valid;
    }

    findUserId(email){
        if(this.state.userType === "PATIENT"){
            for (let i = 0; i < this.patients.length; i++) {
                if(this.patients[i]["user"]["email"] === email)
                    return this.patients[i]["user"]["id"];
            }
        }else{
            for (let i = 0; i < this.caregivers.length; i++) {
                if(this.caregivers[i]["user"]["email"] === email)
                    return this.caregivers[i]["user"]["id"];
            }
        }
        return -1;
    }

    findRoleId(email){
        if(this.state.userType === "PATIENT"){
            for (let i = 0; i < this.patients.length; i++) {
                if(this.patients[i]["user"]["email"] === email)
                    return this.patients[i]["id"];
            }
        }else{
            for (let i = 0; i < this.caregivers.length; i++) {
                if(this.caregivers[i]["user"]["email"] === email)
                    return this.caregivers[i]["id"];
            }
        }
        return -1;
    }

    findMedicationPlans(email){
        for (let i = 0; i < this.patients.length; i++) {
            if(this.patients[i]["user"]["email"] === email)
                return this.patients[i]["medicationPlans"];
        }
        return [];
    }

    findPatientsList(email){
        for (let i = 0; i < this.caregivers.length; i++) {
            if(this.caregivers[i]["user"]["email"] === email)
                return this.caregivers[i]["patients"];
        }
        return [];
    }

    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;
        let check_value = this.check_form(name,value);
        console.log(name + " " + value + " " + check_value);
        let auxState = this.state.formControls;
        let medicalR = "";
        switch(name){
            case "userType": this.setState({userType:value});break;
            case "userEmail":{let userId = this.findUserId(value),roleId=this.findRoleId(value);this.setState({userEmail:value,userId:userId,roleId:roleId});console.log("User id:" + userId +" roleid:"+roleId);break;}
            case "firstName": {auxState.firstName.value = value; auxState.firstName.touched = true; auxState.firstName.valid = check_value;break;}
            case "lastName": {auxState.lastName.value = value; auxState.lastName.touched = true; auxState.lastName.valid = check_value;break;}
            case "birthdate": {auxState.birthdate.value = value; auxState.birthdate.touched = true; auxState.birthdate.valid = check_value;break;}
            case "gender": {auxState.gender.value = value; auxState.gender.touched = true; auxState.gender.valid = check_value;break;}
            case "address": {auxState.adress.value = value; auxState.adress.touched = true; auxState.adress.valid = check_value;break;}
            case "email": {auxState.email.value = value; auxState.email.touched = true; auxState.email.valid = check_value;break;}
            case "password": {auxState.password.value = value; auxState.password.touched = true; auxState.password.valid = check_value;auxState.passwordConfirm.valid = check_value;break;}
            case "passwordConfirm": {auxState.passwordConfirm.value = value; auxState.passwordConfirm.touched = true; auxState.passwordConfirm.valid = check_value;auxState.password.valid = check_value;break;}
            case "medicalRecord": medicalR=value; break;
            default: break;
        }

        if(this.state.userType === "PATIENT" && name === "medicalRecord"){
            auxState.firstName.touched = true;
            auxState.medicalRecord.valid = check_value;
            auxState.medicalRecord.value = medicalR;
        }

        if(name === "userEmail"){
            this.setState({medicationPlans:this.findMedicationPlans(value),patientsList:this.findPatientsList(value)});
        }

        let formvalid = this.checkAllFields();
        if(this.state.userType === "PATIENT")
            formvalid = formvalid && auxState.medicalRecord.valid;
        if(this.state.userId === "" || this.state.userId === -1)
            formvalid = false;
        this.setState({formControls : auxState,formIsValid:formvalid});

        console.log(this.state.formControls);

    };

    updatePatient(patient) {
        return API_PATIENT.updatePatient(patient, (result, status, error) => {
            if (result !== null ) {
                console.log("Successfully inserted patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }
    updateCaregiver(caregiver) {
        return API_CAREGIVER.updateCaregiver(caregiver, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted caregiver with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {

        let patient,caregiver;
        if(this.state.userType === "PATIENT"){
            patient = {
                id:this.state.roleId,
                user : {
                    id:this.state.userId,
                    firstName: this.state.formControls.firstName.value,
                    lastName: this.state.formControls.lastName.value,
                    birthdate: this.state.formControls.birthdate.value,
                    gender: this.state.formControls.gender.value,
                    adress: this.state.formControls.adress.value,
                    email: this.state.formControls.email.value,
                    password: this.state.formControls.password.value,
                    role: "PATIENT"
                },
                medicalRecord: this.state.formControls.medicalRecord.value,
                medicationPlans: this.state.medicationPlans
            }
            this.updatePatient(patient);
            console.log(patient);
        }else{
            caregiver = {
                id:this.state.roleId,
                user : {
                    id:this.state.userId,
                    firstName: this.state.formControls.firstName.value,
                    lastName: this.state.formControls.lastName.value,
                    birthdate: this.state.formControls.birthdate.value,
                    gender: this.state.formControls.gender.value,
                    adress: this.state.formControls.adress.value,
                    email: this.state.formControls.email.value,
                    password: this.state.formControls.password.value,
                    role: "CAREGIVER"
                },
                patients: this.state.patientsList
            }
            console.log(caregiver);
            this.updateCaregiver(caregiver);
        }

    }

    createSelectItemsUsers() {

        let items = [];
        if(this.state.userType === "PATIENT") {
            for (let i = 0; i < this.patients.length; i++) {

                items.push(<option
                    value={this.patients[i]["user"]["email"]}>{this.patients[i]["user"]["email"]}</option>);
            }
        }else {
            for (let i = 0; i < this.caregivers.length; i++) {

                items.push(<option
                    value={this.caregivers[i]["user"]["email"]}>{this.caregivers[i]["user"]["email"]}</option>);
            }
        }

        return items;
    }

    render() {
        return (
            <div>


                <FormGroup id='userType'>
                    <label htmlFor='userTypeField'> User type: </label>
                    <select name="userType" id="userTypeField" onChange={this.handleChange}>
                        <option value="PATIENT"> PATIENT </option>
                        <option value="CAREGIVER"> CAREGIVER </option>
                    </select>
                </FormGroup>

                <FormGroup id='userEmail'>
                    <label htmlFor='userEmailField'> User Email: </label>
                    <select name="userEmail" id="userEmailField" onChange={this.handleChange}>
                        {this.createSelectItemsUsers()}
                    </select>
                </FormGroup>

                <FormGroup id='firstName'>
                    <Label for='firstNameField'> First name: </Label>
                    <Input name='firstName' id='firstNameField'
                           placeholder={this.state.formControls.firstName.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.firstName.value}
                           touched={this.state.formControls.firstName.touched ? 1 : 0}
                           valid={this.state.formControls.firstName.valid}
                           required
                    />
                    {this.state.formControls.firstName.touched && !this.state.formControls.firstName.valid &&
                    <div className={"error-message row"}> *First name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='lastName'>
                    <Label for='lastNameField'> Last name: </Label>
                    <Input name='lastName' id='lastNameField' placeholder={this.state.formControls.lastName.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.lastName.value}
                           touched={this.state.formControls.lastName.touched ? 1 : 0}
                           valid={this.state.formControls.lastName.valid}
                           required
                    />
                    {this.state.formControls.lastName.touched && !this.state.formControls.lastName.valid &&
                    <div className={"error-message row"}> *Last name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='birthdate'>
                    <Label htmlffor='birthdateField'> Birthdate: </Label>
                    <Input type="date" name='birthdate' id='birthdateField'
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.birthdate.value}
                           touched={this.state.formControls.birthdate.touched ? 1 : 0}
                           valid={this.state.formControls.birthdate.valid}
                           required />
                    {this.state.formControls.birthdate.touched && !this.state.formControls.birthdate.valid &&
                    <div className={"error-message row"}> *Invalid birthdate</div>}
                </FormGroup>

                <FormGroup id='gender'>
                    <label htmlFor='genderField'> Gender: </label>
                    <select name="gender" id="genderField" onChange={this.handleChange}>
                        <option value="MALE"> Male</option>
                        <option value="FEMALE"> Female</option>
                    </select>
                </FormGroup>


                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.adress.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.adress.value}
                           touched={this.state.formControls.adress.touched ? 1 : 0}
                           valid={this.state.formControls.adress.valid}
                           required
                    />
                    {this.state.formControls.adress.touched && !this.state.formControls.adress.valid &&
                    <div className={"error-message row"}> *Address must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='email'>
                    <Label for='emailField'> Email: </Label>
                    <Input name='email' id='emailField' placeholder={this.state.formControls.email.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.email.value}
                           touched={this.state.formControls.email.touched ? 1 : 0}
                           valid={this.state.formControls.email.valid}
                           required
                    />
                    {this.state.formControls.email.touched && !this.state.formControls.email.valid &&
                    <div className={"error-message"}> * Email must have a valid format</div>}
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' type="password"
                           onChange={this.handleChange}
                           required
                    />
                </FormGroup>

                <FormGroup id='passwordConfirm'>
                    <Label for='passwordConfirmField'> Password Confirmation: </Label>
                    <Input name='passwordConfirm' id='passwordConfirmField' type="password"
                           onChange={this.handleChange}
                           required
                    />
                    {this.state.formControls.password.touched && this.state.formControls.passwordConfirm.touched
                    && !this.state.formControls.password.valid && !this.state.formControls.passwordConfirm.valid &&
                    <div className={"error-message"}> * Passwords do not match</div>}
                </FormGroup>
                {this.state.userType === "PATIENT" &&
                <FormGroup id='medicalRecord'>
                    <Label for='medicalRecordField'> Medical Record: </Label>
                    <Input name='medicalRecord' id='medicalRecordField'
                           placeholder={this.state.formControls.medicalRecord.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medicalRecord.value}
                           touched={this.state.formControls.medicalRecord.touched ? 1 : 0}
                           valid={this.state.formControls.medicalRecord.valid}
                           required
                    />
                    {this.state.formControls.medicalRecord.touched && !this.state.formControls.medicalRecord.valid &&
                    <div className={"error-message row"}> *Medical Record must not be empty </div>}
                </FormGroup>
                }
                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default UpdateUserForm;
