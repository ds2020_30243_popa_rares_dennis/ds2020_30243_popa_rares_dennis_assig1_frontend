import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Row} from "reactstrap";
import * as API_CAREGIVER from "../api/caregiver-api"

class RemovePatientFromCaregiver extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandlerRemovePatientFromCaregiver;
        this.caregivers = this.props.caregivers;
        this.caregiverEmailAux = this.caregivers.length > 0 ? this.caregivers[0]["user"]["email"] : "";
        this.patientEmailAux = (this.caregivers.length > 0) ? (this.caregivers[0]["patients"].length > 0 ? this.caregivers[0]["patients"][0]["user"]["email"] : "") : "";
        this.state = {
            errorStatus: 0,
            error: null,
            caregiverEmail: this.caregiverEmailAux,
            patientEmail: this.patientEmailAux,
            formIsValid: (this.patientEmailAux !== "" && this.caregiverEmailAux !== "") ? true : false,

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    findCaregiverId(email) {
        for (let i = 0; i < this.caregivers.length; i++) {
            if (this.caregivers[i]["user"]["email"] === email) {
                return this.caregivers[i]["id"];
            }
        }
        return -1;
    }

    findPatientId(email) {
        for (let i = 0; i < this.caregivers.length; i++) {
            for (let j = 0; j < this.caregivers[i]["patients"].length; j++) {
                if (this.caregivers[i]["patients"][j]["user"]["email"] === email) {
                    return this.caregivers[i]["patients"][j]["id"];
                }
            }
        }
        return -1;
    }

    findFirstPatientEmail(caregiverEmail){
        for (let i = 0; i < this.caregivers.length; i++) {
            for (let j = 0; j < this.caregivers[i]["patients"].length; j++) {
                if (this.caregivers[i]["user"]["email"] === caregiverEmail) {
                    return this.caregivers[i]["patients"][j]["user"]["email"];
                }
            }
        }
        return "";
    }

    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;
        let auxState = this.state;
        if (name === "caregiverEmail") {
            auxState.caregiverEmail = value;
            auxState.patientEmail = this.findFirstPatientEmail(value);
        }
        if (name === "patientEmail") {
            auxState.patientEmail = value;
        }

        if (auxState.patientEmail !== "" && auxState.caregiverEmail !== "") {
            auxState.formIsValid = true;
        }else{
            auxState.formIsValid = false;
        }

        this.setState(auxState);
    };

    removePatientFromCaregiver(caregiverId, patientId) {
        return API_CAREGIVER.removePatientFromCaregiver(caregiverId, patientId, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        console.log(this.state);
        let caregiverId = this.findCaregiverId(this.state.caregiverEmail);
        let patientId = this.findPatientId(this.state.patientEmail);
        console.log("remove call " + caregiverId + " " + patientId);
        this.removePatientFromCaregiver(caregiverId, patientId);
    }

    createSelectItemsCaregivers() {
        console.log("Caregivers list ....");
        console.log(this.caregivers);
        let items = [];
        for (let i = 0; i < this.caregivers.length; i++) {
            items.push(<option
                value={this.caregivers[i]["user"]["email"]}>{this.caregivers[i]["user"]["email"]}</option>);
        }

        return items;
    }

    createSelectItemsPatients(caregiverEmail) {
        let items = [];
        for (let i = 0; i < this.caregivers.length; i++) {
            if (this.caregivers[i]["user"]["email"] === caregiverEmail) {
                for (let j = 0; j < this.caregivers[i]["patients"].length; j++) {
                    items.push(<option
                        value={this.caregivers[i]["patients"][j]["user"]["email"]}>{this.caregivers[i]["patients"][j]["user"]["email"]}</option>);
                }
            }
        }
        return items;
    }

    render() {
        return (
            <div>

                <FormGroup id='caregiverEmail'>
                    <label htmlFor='caregiverEmailField'>Caregiver Email: </label>
                    <select name="caregiverEmail" id="caregiverEmailField" onChange={this.handleChange}>
                        {this.createSelectItemsCaregivers()}
                    </select>
                </FormGroup>

                <FormGroup id='patientEmail'>
                    <label htmlFor='patientEmailField'> Patient Email: </label>
                    <select name="patientEmail" id="patientEmailField" onChange={this.handleChange}>
                        {this.createSelectItemsPatients(this.state.caregiverEmail)}
                    </select>
                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default RemovePatientFromCaregiver;
