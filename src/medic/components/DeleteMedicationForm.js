import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Row} from "reactstrap";
import * as API_MEDICATION from "../api/medication-api"

class DeleteMedicationForm extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandlerDeleteMedication;
        this.medications = this.props.medications;

        let medicationNameAux = (this.medications.length > 0) ? this.medications[0]["name"] : "";
        let medicationIdAux = medicationNameAux !== "" ? this.findMedicationId(medicationNameAux):"";

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: (medicationIdAux !== ""),
            medicationId: medicationIdAux,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    findMedicationId(name) {
        for (let i = 0; i < this.medications.length; i++) {
            if (this.medications[i]["name"] === name)
                return this.medications[i]["id"];
        }
        return -1;
    }


    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;
        console.log(name + " " + value + " ");

        if(name === "medicationName"){
            let id = this.findMedicationId(value);
            if(id !== -1)
                this.setState({medicationId:id,formIsValid:true});
        }

    };

    deleteMedication(medication) {
        return API_MEDICATION.deleteMedication(medication, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }


    handleSubmit() {
        this.deleteMedication(this.state.medicationId);
    }

    createSelectItemsMedication() {
        let items = [];
        for (let i = 0; i < this.medications.length; i++) {
            items.push(<option
                value={this.medications[i]["name"]}>{this.medications[i]["name"]}</option>);
        }
        return items;
    }

    render() {
        return (
            <div>

                <FormGroup id='medicationName'>
                    <label htmlFor='medicationNameField'>Medication name: </label>
                    <select name="medicationName" id="medicationNameField" onChange={this.handleChange}>
                        {this.createSelectItemsMedication()}
                    </select>
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default DeleteMedicationForm;
