import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import * as API_MEDICATIONPLAN from "../api/medicationPlan-api"

class UpdateMedicationPlanForm extends React.Component {

    constructor(props) {

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandlerUpdateMedicationPlan;
        this.check_form = this.check_form.bind(this);
        this.medicationPlans = this.props.medicationPlans;
        let planIdAux = (this.medicationPlans.length > 0) ? this.medicationPlans[0]["id"] : -1;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            planId: planIdAux,
            formControls: {
                startTime: {
                    value: '15:00',
                    valid: false,
                    touched: false,
                },
                endTime: {
                    value: '15:30',
                    valid: false,
                    touched: false,
                },
                startDate: {
                    value: '',
                    valid: false,
                    touched: false,
                },
                endDate: {
                    value: '',
                    valid: false,
                    touched: false,
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    check_form() {
        if(this.state.formControls.startDate.touched && this.state.formControls.endDate.touched)
        {
            let start = new Date(this.state.formControls.startDate.value);
            let end = new Date(this.state.formControls.endDate.value);
            let auxState = this.state.formControls;
            auxState.startDate.valid = (end > start);
            auxState.endDate.valid = (end > start);
            this.setState({formControls:auxState});
        }
        if(this.state.formControls.startTime.touched && this.state.formControls.endTime.touched)
        {
            let timeexp1 = "2020-11-04 " + this.state.formControls.startTime.value;
            let timeexp2 = "2020-11-04 " + this.state.formControls.endTime.value;
            let dateTime1 = new Date(timeexp1);
            let dateTime2 = new Date(timeexp2);
            let auxState = this.state.formControls;
            auxState.startTime.valid = (dateTime2 > dateTime1);
            auxState.endTime.valid = (dateTime2 > dateTime1);
            this.setState({formControls:auxState});
        }
    }

    checkAllFields() {
        return this.state.formControls.startTime.valid && this.state.formControls.endTime.valid && this.state.formControls.startDate.valid && this.state.formControls.endDate.valid;
    }

    handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;
        let auxState = this.state.formControls;

        if(name === "startDate"){
            auxState.startDate.touched = true;
            auxState.startDate.value = value;
        }
        if(name === "endDate"){
            auxState.endDate.touched = true;
            auxState.endDate.value = value;
        }
        if(name === "startTime"){
            auxState.startTime.touched = true;
            auxState.startTime.value = value;
        }
        if(name === "endTime"){
            auxState.endTime.touched = true;
            auxState.endTime.value = value;
        }
        if(name === "medicationPlanId"){
            this.setState({planId:value});
        }

        this.setState({formControls:auxState});
        this.check_form();
        let valid = this.checkAllFields();
        this.setState({formIsValid:valid});
        console.log(name + " " + value + " " );
    };

    updateMedicationPlan(medicationPlan) {
        return API_MEDICATIONPLAN.updateMedicationPlan(medicationPlan, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let medicationPlan = {
            id:this.state.planId,
            medications: this.findMedicationListById(this.state.planId),
            startTime: "2019-10-15 " + this.state.formControls.startTime.value,
            endTime: "2019-10-15 " + this.state.formControls.endTime.value,
            startDate: this.state.formControls.startDate.value,
            endDate: this.state.formControls.endDate.value
        }
        console.log(medicationPlan);
        this.updateMedicationPlan(medicationPlan);
    }

    findMedicationListById(id){
        let id_int = parseInt(id);
        for (let i = 0; i < this.medicationPlans.length; i++) {
            if(this.medicationPlans[i]["id"] === id_int){
                return this.medicationPlans[i]["medications"];
            }
        }
        return [];
    }

    createSelectItemsMedicationPlans() {

        let items = [];
        for (let i = 0; i < this.medicationPlans.length; i++) {
            items.push(<option
                value={this.medicationPlans[i]["id"]}>{this.medicationPlans[i]["id"]}</option>);
        }

        return items;
    }

    render() {
        return (
            <div>

                <FormGroup id='medicationPlanId'>
                    <label htmlFor='medicationPlanIdField'> Medication Plan Id: </label>
                    <select name="medicationPlanId" id="medicationPlanIdField" onChange={this.handleChange}>
                        {this.createSelectItemsMedicationPlans()}
                    </select>
                </FormGroup>

                <FormGroup id='startDate'>
                    <Label htmlffor='startDateField'>Start Date: </Label>
                    <Input type="date" name='startDate' id='startDateField'
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.startDate.value}
                           touched={this.state.formControls.startDate.touched ? 1 : 0}
                           valid={this.state.formControls.startDate.valid}
                           required />
                </FormGroup>

                <FormGroup id='endDate'>
                    <Label htmlffor='endDateField'>End Date: </Label>
                    <Input type="date" name='endDate' id='endDateField'
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.endDate.value}
                           touched={this.state.formControls.endDate.touched ? 1 : 0}
                           valid={this.state.formControls.endDate.valid}
                           required />
                    {this.state.formControls.startDate.touched && !this.state.formControls.startDate.valid && this.state.formControls.endDate.touched && !this.state.formControls.endDate.valid &&
                    <div className={"error-message row"}> *Invalid Dates</div>}
                </FormGroup>

                <FormGroup id='startTime'>
                    <Label htmlffor='startTimeField'>Start Time: </Label>
                    <Input type="time" name='startTime' id='startTimeField'
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.startTime.value}
                           touched={this.state.formControls.startTime.touched ? 1 : 0}
                           valid={this.state.formControls.startTime.valid}
                           required />
                </FormGroup>

                <FormGroup id='endTime'>
                    <Label htmlffor='endTimeField'> End Time: </Label>
                    <Input type="time" name='endTime' id='endTimeField'
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.endTime.value}
                           touched={this.state.formControls.endTime.touched ? 1 : 0}
                           valid={this.state.formControls.endTime.valid}
                           required />
                    {this.state.formControls.startTime.touched && !this.state.formControls.startTime.valid && this.state.formControls.endTime.touched && !this.state.formControls.endTime.valid &&
                    <div className={"error-message row"}> *Invalid Time Interval</div>}
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default UpdateMedicationPlanForm;
