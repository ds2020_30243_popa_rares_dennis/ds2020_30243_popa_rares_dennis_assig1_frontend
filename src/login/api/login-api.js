import {HOST} from '../../commons/api/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    user: '/user'
};


function checkLogin(email,password,callback) {
    let request = new Request(HOST.backend_api + endpoint.user + '/checkLogin/'  + email + '/' + password, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    checkLogin
};